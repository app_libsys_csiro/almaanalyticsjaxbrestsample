package service;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import service.dto.jaxb.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AlmaRestService
{
    
    private RestTemplate restTemplate;
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private static final int READ_TIMEOUT = 90000; // Read timeout in milliseconds
	private static final int ONE_THOUSAND =1000;
	

	/**
	 * Constructor for setting up the RestService
	 */
	public AlmaRestService()
	{		
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setReadTimeout(READ_TIMEOUT);
        factory.setConnectTimeout(ONE_THOUSAND);
        restTemplate = new RestTemplate(factory);
	}	

	public Report getReport(String url) throws RestClientException
	{
		try 
		{
			RestTemplate restTemplate = new RestTemplate();
		    Report report = restTemplate.getForObject(url, Report.class);
			return report;
		}	
			catch(Exception e)
			{
				log.error("Error from call to Analytics: " + e.getMessage());
				throw new RestClientException(e.getMessage());
			}
	}
	
}


