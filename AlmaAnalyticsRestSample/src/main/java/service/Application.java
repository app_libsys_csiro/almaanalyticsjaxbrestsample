package service;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import service.dto.jaxb.Report;
import service.dto.jaxb.Row;
import service.dto.jaxb.Rowset;

/**Simple example application to show jaxb objects to retrieve Alma analytics data.
 * Retrieves data down to the Row level.
 */

@SpringBootApplication
public class Application implements CommandLineRunner {
	
    //The path within Alma Analytics to your report - change for your environment.
	private static String REPORT = "path=/shared/......./Reports/ZZUserID4Data61";
    //Your Alma reports url - change for your environment.
	private static String ALMA = "https://....../almaws/v1/analytics/reports?";
    private static String APIKEY = "ADD YOUR API KEY HERE";
    private static String LIMIT = "25";
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public static void main(String args[]) {
        SpringApplication.run(Application.class);
    }

    @Override
    public void run(String... strings) throws Exception {
    	
    	AlmaRestService service = new AlmaRestService();
		getReports(service, log);	
    }	
    
    
    private static void getReports(AlmaRestService service, Logger log)
    {
    	
  		String url = ALMA + REPORT + "&limit=" + LIMIT + "&apikey=" + APIKEY;
			
			List<Row> rowList = new ArrayList<Row>();
			
			log.info("Running report: " + url);
			//first run do not use resumption token
			Report reportFirstRun = service.getReport(url);
			
			if(reportFirstRun != null)
			{
				if(reportFirstRun.getQueryResult().getResultXml().getRowset().getRow() != null)
				{	
					rowList.addAll(reportFirstRun.getQueryResult().getResultXml().getRowset().getRow());
				}
				
				//if there's more use the resumption token
				if(reportFirstRun.getQueryResult().getIsFinished().equalsIgnoreCase("false"))
				{
					boolean token = true;
					while(token)
					{
						url = ALMA + "token=" + 
					          reportFirstRun.getQueryResult().getResumptionToken() +
					          "&limit=" +  LIMIT + "&apikey=" + APIKEY;
						log.info("Retrieving next set of results: " + url);
						Report reportTokened = service.getReport(url);
						rowList.addAll(reportTokened.getQueryResult().getResultXml().getRowset().getRow());
	                    //quite the loop 
						if(reportTokened.getQueryResult().getIsFinished().equalsIgnoreCase("true"))
						{
							token = false;
						}
						
					}
				}
				
				Rowset setOut = new Rowset();
				setOut.setRow(rowList);
				System.out.println(jaxbObjectToXML(setOut));
			}				
    }		
    
    /**
     * Used to write output to console for testing purposes.
     * @param rx - a populated Rowset object
     * @return xmlString - formatted XML representation of the object
     */
    private static String jaxbObjectToXML(Rowset rx) {
        String xmlString = "";
        try 
        {
            JAXBContext context = JAXBContext.newInstance(Rowset.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); // To format XML
            StringWriter sw = new StringWriter();
            m.marshal(rx, sw);
            xmlString = sw.toString();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return xmlString;
    }
}