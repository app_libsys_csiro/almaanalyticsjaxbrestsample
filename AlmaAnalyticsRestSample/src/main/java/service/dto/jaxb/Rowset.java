package service.dto.jaxb;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.NONE)

@XmlRootElement(name = "rowset")
public class Rowset {

    @XmlElement(name = "Row", required = true)
    protected List<Row> row;


    public List<Row> getRow() {
        if (row == null) {
            row = new ArrayList<Row>();
        }
        return this.row;
    }
    
    public void setRow(List<Row> rowList) {
    	this.row = rowList;
    }
}
