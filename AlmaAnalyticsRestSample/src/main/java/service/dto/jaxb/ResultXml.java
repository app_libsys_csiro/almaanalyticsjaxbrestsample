package service.dto.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rowset"
})
public class ResultXml {

    @XmlElement(name="rowset", namespace="urn:schemas-microsoft-com:xml-analysis:rowset", required = true)
    protected Rowset rowset;

    public Rowset getRowset() {
        return rowset;
    }

    public void setRowset(Rowset value) {
        this.rowset = value;
    }
}