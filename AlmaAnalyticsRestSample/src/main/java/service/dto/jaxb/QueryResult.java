package service.dto.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resumptionToken",
    "isFinished",
    "resultXml"
})
public class QueryResult {

    @XmlElement(name = "ResumptionToken", required = true)
    protected String resumptionToken;
    @XmlElement(name = "IsFinished", required = true)
    protected String isFinished;
    @XmlElement(name = "ResultXml", required = true)
    protected ResultXml resultXml;

    public String getResumptionToken() {
        return resumptionToken;
    }

    public void setResumptionToken(String value) {
        this.resumptionToken = value;
    }

    public String getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(String value) {
        this.isFinished = value;
    }

    public ResultXml getResultXml() {
        return resultXml;
    }

    public void setResultXml(ResultXml value) {
        this.resultXml = value;
    }
}
