# README #

Alma Analytics Rest Sample

### What is this repository for? ###
A Sample Spring Boot/REST application used to show what jaxb pojos can be used to retrieve an Alma Analytics report.
Version: 0.1.0

### How do I get set up? ###
This git repository is an exported Eclipse/Maven project.

### Who do I talk to? ###
antony.tomasovic@csiro.au